import express, { Request, Response } from "express";

const app = express();
app.get("/ping", (_: Request, res: Response) => {
  res.send("pong");
});

export const server = app.listen(3000, "localhost");
export default app;
