import supertest from "supertest";
import app, { server } from "./main";

describe("/ping", () => {
  afterAll(() => {
    server.close();
  });
  it("returns pong as a response", async () => {
    const response = await supertest(app).get("/ping");
    expect(response.text).toBe("pong");
  });
});
